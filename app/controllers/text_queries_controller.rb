class TextQueriesController < ApplicationController


  def new

  end

  def create

    @tq = TextQuery.new(text_q_params)

    if @tq.save

      respond_to do |format|

        format.html
        format.js

      end
    end

  end



  private

  def text_q_params
    params.require(:text_query).permit(:text_query)
  end

end



