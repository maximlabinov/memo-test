class CreateTextQueries < ActiveRecord::Migration[5.2]
  def change
    create_table :text_queries do |t|

      t.string :text_query

      t.timestamps
    end
  end
end
